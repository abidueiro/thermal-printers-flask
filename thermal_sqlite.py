# importing Flask and other modules
from flask import Flask, request, render_template, send_from_directory, redirect, url_for
import os
import io
import codecs
import sqlite3
import time
import subprocess
import dotenv

dotenv.load_dotenv("env")
next_pass = os.getenv('next_pass')
next_user = os.getenv('next_user')

start = time.time()

# checking if the directory demo_folder 
# exist or not.
if not os.path.exists("bbdd"):
      
# if the demo_folder directory is not present 
# then create it.
    os.makedirs("bbdd")


# Flask constructor
app = Flask(__name__)
db_name = "que_vols_pasi_canodrom.db"

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='')
 
@app.route('/thermal12', methods =["GET", "POST"])
def thermal12():
    if request.method == "POST":
       os.chdir("/home/alfons/Oficina_canodrom/DEVELOPMENT_PROGRAMMING/PYTHON_2023/thermal-printers")
       resposta = request.form.get("lname")
       with open('print12', 'w', encoding="utf-8") as f:
        space = (24 * "\n")
        f.write(resposta+space+".")
        # Create database
        os.chdir("/home/alfons/Oficina_canodrom/DEVELOPMENT_PROGRAMMING/PYTHON_2023/thermal-printers/bbdd")
        table_name = "thermal12"
        db = sqlite3.connect(db_name)
        # write sqlite db
        cursor = db.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + table_name + " (URLID INTEGER PRIMARY KEY AUTOINCREMENT, Resposta REAL, Time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)")
        cursor.execute("INSERT INTO " + table_name + " (Resposta) VALUES(?)", (resposta,))
        db.commit()
        cursor.close()
        db.close()
        subprocess.call(["nextcloudcmd", "/home/alfons/Oficina_canodrom/DEVELOPMENT_PROGRAMMING/PYTHON_2023/thermal-printers/bbdd", f"https://{next_user}:{next_pass}=@oficina.canodrom.barcelona"])       
       os.system('/usr/bin/lp -d thermal12 print12')
       return redirect(url_for('answer'))
    return render_template("thermal12.html")

@app.route('/thermal13', methods =["GET", "POST"])
def thermal13():
    if request.method == "POST":
       os.chdir("/home/alfons/Oficina_canodrom/DEVELOPMENT_PROGRAMMING/PYTHON_2023/thermal-printers")
       resposta = request.form.get("lname")
       with open('print13', 'w', encoding="utf-8") as f:
        space = (24 * "\n")
        f.write(resposta+space+".")
        # Create database
        os.chdir("/home/alfons/Oficina_canodrom/DEVELOPMENT_PROGRAMMING/PYTHON_2023/thermal-printers/bbdd")
        table_name = "thermal13"
        db = sqlite3.connect(db_name)
        # write sqlite db
        cursor = db.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS " + table_name + " (URLID INTEGER PRIMARY KEY AUTOINCREMENT, Resposta REAL, Time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)")
        cursor.execute("INSERT INTO " + table_name + " (Resposta) VALUES(?)", (resposta,))
        db.commit()
        cursor.close()
        db.close()
       os.system('/usr/bin/lp -d thermal13 print13')
       return redirect(url_for('answer'))
    return render_template("thermal13.html")

@app.route('/thermal14', methods =["GET", "POST"])
def thermal14():
    if request.method == "POST":
       resposta = request.form.get("lname")
       with open('print14', 'w', encoding="utf-8") as f:
        space = (24 * "\n")
        f.write(resposta+space+".")
       os.system('/usr/bin/lp -d thermal14 print14')
       return redirect(url_for('answer'))
    return render_template("thermal14.html")

@app.route('/thermal15', methods =["GET", "POST"])
def thermal15():
    if request.method == "POST":
       resposta = request.form.get("lname")
       with open('print15', 'w', encoding="utf-8") as f:
        space = (24 * "\n")
        f.write(resposta+space+".")
       os.system('/usr/bin/lp -d thermal15 print15')
       return redirect(url_for('answer'))
    return render_template("thermal15.html")

@app.route('/answer', methods =["GET", "POST"])
def answer():
    return render_template("answer.html")

if __name__=='__main__':
   app.run(host='0.0.0.0')



