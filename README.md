# thermal printers

Aplicació web per interactuar amb impressores tèrmiques Iggual TP8002. 

## Requeriments

- servidor CUPS
- Flask, Uwsgi
- Servidor web
- Generador codis qr
- Per al servidor: Sistema operatiu GNU/Linux, pot servir un contenidor LXC amb mínim 512mb de RAM, 1cpu i 10gb de HDD 

![image](/uploads/7c82b97e44e38a1b0b414fc7b1d9fc71/image.png)


![image](/uploads/6963d078096f5fb42872fd63e4c69cb6/image.png)
